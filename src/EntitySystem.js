import { Bag } from './Bag';

export class EntitySystem {
  __componentTypes = [];
  __entities = new Bag();
  __ecsInstance = null;

  get ecsInstance() {
    return this.__ecsInstance;
  }

  set ecsInstance(value) {
    this.__ecsInstance = value;
  }

  get componentTypes(){
    return this.__componentTypes;
  }

  set componentTypes(value){
    this.__componentTypes = value;
  }

  loadContent() {
    this.preLoadContent(this.__entities);
  }

  removeEntity(entity) {
    if (this.__entities.remove(entity)) this.removed(entity);
  }

  addEntity(entity) {
    this.__entities.add(entity);
    this.added(entity);
  }

  cleanSystem() {
    this.cleanUp(this.__entities);
    this.__entities.clear();
  }

  processAll() {
    this.begin();
    this.processEntities(this.__entities);
    this.end();
  }

  processEntities(entities) {
    for (let i = 0; i < entities.count; i++) {
      this.process(entities.get(i));
    }
  }

  shouldProcess() {
    return true;
  }

  //overloadable functions
  initialize() {}
  preLoadContent(entities) {}
  removed(entity) {}
  added(entity) {}
  cleanUp(entities) {}
  begin() {}
  end() {}
  process(entity) {}
}
