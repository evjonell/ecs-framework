import { Bag } from 'Bag';
import { Component } from 'Component';
import { ComponentManager } from 'ComponentManager';
import { ComponentMapper } from 'ComponentMapper';
import { EcsInstance } from 'EcsInstance';
import { Entity } from 'Entity';
import { EntityManager } from 'EntityManager';
import { EntitySystem } from 'EntitySystem';
import { GroupManager } from 'GroupManager';
import { SystemManager } from 'SystemManager';
import { TagManager } from 'TagManager';

export default {
  Bag,
  Component,
  ComponentManager,
  ComponentMapper,
  EcsInstance,
  Entity,
  EntityManager,
  EntitySystem,
  GroupManager,
  SystemManager,
  TagManager
};
