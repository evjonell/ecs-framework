export class Bag {
  __data = [];
  __count = 0;

  constructor(capacity = 16) {
    this.__data = new Array(capacity);
    this.__count = 0;
  }

  get capcity() {
    return this.__data.length;
  }

  get isEmpty() {
    return this.__count === 0;
  }

  get count() {
    return this.__count;
  }

  get(index) {
    return this.__data[index];
  }

  set(index, value) {
    if (index >= this.__data.length) {
      this.grow(index * 2);
      this.__count = index + 1;
    } else if (index >= this.__count) {
      this.__count = index + 1;
    }
    this.__data[index] = value;
  }

  add(element) {
    if (this.__count >= this.__data.length) {
      this.grow();
    }
    this.__data[this.__count] = element;
    this.__count++;
  }

  addRange(bag) {
    for (let i = 0; bag.length > i; i++) {
      this.add(bag.get(i));
    }
  }

  clear() {
    this.__data = this.__data.map(_ => undefined);
    this.__count = 0;
  }

  contains(element) {
    return this.__data.includes(element);
  }

  remove(element) {
    const index = this.__data.indexOf(element);
    if (index >= 0) {
      const item = this.__data[index];
      this.__count--;
      this.__data[index] = this.__data[this.__count];
      this.__data[this.__count] = undefined;
      return item;
    } else {
      return null;
    }
  }

  removeAt(index) {
    if (index < this.__data.length) {
      const item = this.__data[index];
      this.__count--;
      this.__data[index] = this.__data[this.__count];
      this.__data[this.__count] = undefined;
      return item;
    } else {
      return null;
    }
  }

  removeLast() {
    this.__count--;
    const item = this.__data[this.__count];
    this.__data[this.__count] = undefined;
    return item;
  }

  grow(size = 1.5 * this.__data.length + 1) {
    this.__data = [...this.__data, new Array(size - this.__data.length)];
  }
}
