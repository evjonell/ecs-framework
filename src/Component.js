export class Component {
  static __type = -1;

  static get type(){
    return this.__type;
  }

  static set type(value){
    this.__type = value;
  }

  get type(){
    return this.constructor.__type;
  }
}
