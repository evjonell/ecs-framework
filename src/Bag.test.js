import { Bag } from 'Bag';

describe('Bag', () => {
  it('should instantiate without crashing', () => {
    const bag = new Bag();
    expect(bag).toBeDefined();
  });

  it('should initialize data and count on creation', () => {
    const bag = new Bag();
    expect(bag.__data).toBeDefined();
    expect(bag.__data.length).toEqual(16);
    expect(bag.__count).toBeDefined();
    expect(bag.__count).toEqual(0);
  });

  it('should initialize data to passed capacity', () => {
    const bag = new Bag(32);
    expect(bag.__data.length).toEqual(32);
  });

  it('should support various getters', () => {
    const bag = new Bag();
    expect(bag.capcity).toEqual(bag.__data.length);
    expect(bag.isEmpty).toEqual(bag.__count === 0);
    expect(bag.count).toEqual(bag.__count);
  });

  it('return undefined values by default', () => {
    const bag = new Bag();
    expect(bag.get(5)).toBeUndefined();
  });

  it('should set values', () => {});

  it('should increase count when setting a value', () => {});

  it('should decrease count when removing a value', () => {});

  it('should be able to add elements', () => {});

  it('should be able to add bags', () => {});

  it('should remove all values on clear', () => {});

  it('should see if it contains a value', () => {});

  it('should be able to remove a specific element', () => {});

  it('should be able to remove an element at an index', () => {});

  it('should be able to remove the last element', () => {});

  it('should be able to grow the bag', () => {});

  it('should be grow the bag after adding an element beyond data length', () => {});

  it('should be grow the bag after setting an element beyond data length', () => {});
});
